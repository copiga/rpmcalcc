#include <stdio.h>
#include <math.h>

#include "copiga.h"    //v2+ for clearin()

int main()
{
  puts ("1. speed");
  puts ("2. circumference");
  puts ("3. RPM");
  switch(getchar())
    {
    case '1':
      speed();
      break;
    case '2':
      circumference();
      break;
    case '3':
      revs();
      break;
    default:
      main();
      break;
    }
}


int speed()
{
  clearin();
  float revs;
  double circumference, speed;
  puts ("input revs per minute");
  scanf (" %f", &revs);
  clearin();
  puts ("please input circumference in metres");
  scanf (" %lf", &circumference);
  speed = circumference * revs;
  printf ("the speed is %lf metres per minute\n", speed);
}


int circumference()
{
  clearin();
  float revs;
  double speed, circumference;
  puts ("input revs per minute");
  scanf (" %f", &revs);
  clearin();
  puts ("please enter the speed in metres per minute");
  scanf (" %lf", &speed);
  clearin();
  circumference = speed / revs;
  printf ("the circumference is %lf metres\n", circumference);
}


int revs()
{
  clearin();
  float revs;
  double speed, circumference;
  puts ("please enter the speed in metres per minute");
  scanf (" %lf", &speed);
  clearin();
  puts ("please enter the circumference in metres");
  scanf (" %lf", &circumference);
  revs = circumference / speed;
  printf ("%f RPM\n", revs);
}
